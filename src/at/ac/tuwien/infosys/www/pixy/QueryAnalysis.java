package at.ac.tuwien.infosys.www.pixy;

import at.ac.tuwien.infosys.www.pixy.AbstractVulnerabilityAnalysis.InitialTaint;
import at.ac.tuwien.infosys.www.pixy.analysis.dependency.DependencyAnalysis;
import at.ac.tuwien.infosys.www.pixy.analysis.dependency.Sink;
import at.ac.tuwien.infosys.www.pixy.analysis.dependency.graph.*;
import at.ac.tuwien.infosys.www.pixy.automaton.Automaton;
import at.ac.tuwien.infosys.www.pixy.automaton.Transition;
import at.ac.tuwien.infosys.www.pixy.conversion.AbstractTacPlace;
import at.ac.tuwien.infosys.www.pixy.conversion.TacActualParameter;
import at.ac.tuwien.infosys.www.pixy.conversion.TacFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.cfgnodes.AbstractCfgNode;
import at.ac.tuwien.infosys.www.pixy.conversion.cfgnodes.CallBuiltinFunction;
import at.ac.tuwien.infosys.www.pixy.conversion.cfgnodes.CallPreparation;
import at.ac.tuwien.infosys.www.pixy.conversion.cfgnodes.CallUnknownFunction;
import at.ac.tuwien.infosys.www.pixy.sanitation.AbstractSanitationAnalysis;
import at.ac.tuwien.infosys.www.pixy.transduction.MyTransductions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * SQL Injection detection.
 *
 * Note: This class will be instantiated via reflection in GenericTaintAnalysis.createAnalysis. It is registered in
 * MyOptions.VulnerabilityAnalysisInformation.
 *
 * @author Nenad Jovanovic <enji@seclab.tuwien.ac.at>
 */
public class QueryAnalysis extends AbstractVulnerabilityAnalysis {
    /** flag indicating whether to use transducers (are still unstable) */
    private boolean useTransducers = false;
    private List<Integer> lineNumbersOfVulnerabilities;
    private int dependencyGraphCount;
    private int vulnerabilityCount;

    public QueryAnalysis(DependencyAnalysis dependencyAnalysis) {
        super(dependencyAnalysis);
        //this.getIsTainted = !MyOptions.optionI;
    }

    /**
     * Detects vulnerabilities and returns a list with the line numbers of the detected vulnerabilities.
     *
     * @return the line numbers of the detected vulnerabilities
     */
    public List<Integer> detectVulnerabilities() {
        System.out.println();
        System.out.println("*****************");
        System.out.println("Query Analysis BEGIN");
        System.out.println("*****************");
        System.out.println();

        lineNumbersOfVulnerabilities = new LinkedList<>();

        List<Sink> sinks = this.collectSinks();
        Collections.sort(sinks);

        System.out.println("Number of sinks: " + sinks.size());
        System.out.println();

        String fileName = MyOptions.entryFile.getName();

        dependencyGraphCount = 0;
        vulnerabilityCount = 0;
        for (Sink sink : sinks) {
            detectRulesForSink(fileName, sink);
        }
        
        System.out.println("Checking is done.");

        // initial sink count and final graph count may differ (e.g., if some sinks
        // are not reachable)
        if (MyOptions.optionV) {
            System.out.println("Total Graph Count: " + dependencyGraphCount);
        }
        //We don't need to consider about the vulnerability stuff
        //System.out.println("Total Vuln Count: " + vulnerabilityCount);

        System.out.println();
        System.out.println("*****************");
        System.out.println("Query Analysis END");
        System.out.println("*****************");
        System.out.println();

        return lineNumbersOfVulnerabilities;
    }
    
    //Not aware of what this function is used for
	@Override
	public VulnerabilityInformation detectAlternative() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//used for checking for sinks
	@Override
	protected void checkForSink(AbstractCfgNode cfgNodeX, TacFunction traversedFunction, List<Sink> sinks) {
        if (cfgNodeX instanceof CallBuiltinFunction) {
            // builtin function sinks
            CallBuiltinFunction cfgNode = (CallBuiltinFunction) cfgNodeX;
            String functionName = cfgNode.getFunctionName();
            
            checkForSinkHelper(functionName, cfgNode, cfgNode.getParamList(), traversedFunction, sinks);
        } else if (cfgNodeX instanceof CallPreparation) {
            CallPreparation cfgNode = (CallPreparation) cfgNodeX;
            String functionName = cfgNode.getFunctionNamePlace().toString();
            
            // user-defined custom sinks
            checkForSinkHelper(functionName, cfgNode, cfgNode.getParamList(), traversedFunction, sinks);
        } else if (cfgNodeX instanceof CallUnknownFunction) {
        	
        	//unknown functions, like $wpdb->get_results($query)
        	CallUnknownFunction cfgNode = (CallUnknownFunction) cfgNodeX;
            String functionName = cfgNode.getFunctionName().toString();
            checkForSinkHelper(functionName, cfgNode, cfgNode.getParamList(), traversedFunction, sinks);
        }
    }

    private void checkForSinkHelper(String functionName, AbstractCfgNode cfgNode, List<TacActualParameter> paramList, TacFunction traversedFunction, List<Sink> sinks) {
        if (this.vulnerabilityAnalysisInformation.getSinks().containsKey(functionName)) {
            Sink sink = new Sink(cfgNode, traversedFunction);
            for (Integer param : this.vulnerabilityAnalysisInformation.getSinks().get(functionName)) {
                if (paramList.size() > param) {
                    sink.addSensitivePlace(paramList.get(param).getPlace());
                    // add this sink to the list of sensitive sinks
                    sinks.add(sink);
                }
            }
        }
    }
	
    //check the rule during building the automaton
	private void detectRulesForSink(String fileName, Sink sink) {
        Collection<DependencyGraph> dependencyGraphs = dependencyAnalysis.getDependencyGraphsForSink(sink);

        for (DependencyGraph dependencyGraph : dependencyGraphs) {
        	dependencyGraphCount++;
            DependencyGraph sqlGraph = new DependencyGraph(dependencyGraph);
            //AbstractCfgNode cfgNode = dependencyGraph.getRootNode().getCfgNode();
            
            Automaton automaton = this.toAutomaton(sqlGraph, dependencyGraph);
            //toString() is needed otherwise the states won't have different numbers
            automaton.toString();
            if(automaton.hasCycles()) {
            	if(automaton.hasOrs()) {
            		System.out.println("OR cycle(loop type) has been detected!");
        			//System.out.println(((BuiltinFunctionNode) origPre).getLine());
        		}
            }
            System.out.println();
        }
	}
	
	//most of the following code is directly borrowed from SqlAnalysis.java
	//it will be reorganized if new rules will be added or if necessary
	Automaton toAutomaton(DependencyGraph dependencyGraph, DependencyGraph origDependencyGraph) {
        dependencyGraph.eliminateCycles();
        AbstractNode root = dependencyGraph.getRootNode();
        Map<AbstractNode, Automaton> deco = new HashMap<>();
        Set<AbstractNode> visited = new HashSet<>();
        this.decorate(root, deco, visited, dependencyGraph, origDependencyGraph);
        Automaton rootDeco = deco.get(root).clone();

        return rootDeco;
    }
	
	private void decorate(AbstractNode node, Map<AbstractNode, Automaton> deco, Set<AbstractNode> visited, DependencyGraph dependencyGraph, DependencyGraph origDependencyGraph) {
		visited.add(node);
		// if this node has successors, decorate them first (if not done yet)
		List<AbstractNode> successors = dependencyGraph.getSuccessors(node);
		if (successors != null && !successors.isEmpty()) {
			for (AbstractNode succ : successors) {
				if (!visited.contains(succ) && deco.get(succ) == null) {
					decorate(succ, deco, visited, dependencyGraph, origDependencyGraph);
				}
			}
		}
		
		// now that all successors are decorated, we can decorate this node
		Automaton auto = null;
		if (node instanceof NormalNode) {
			NormalNode normalNode = (NormalNode) node;
			if (successors == null || successors.isEmpty()) {
				// this should be a string leaf node
				// this is used to detect the hard-coded a-bunch-of-ORs issues.
				// i. e, the query "select * from tab where id = 1 or id = 2 or id = 3" is implemented in a hard-coded way.
				// for now, I just check if the query contains more than 3 ors. This is not a correct way.
				AbstractTacPlace place = normalNode.getPlace();
				if (place.isLiteral()) {
					//to detect the a-bunch-of-OR case written in plain text
					/*String[] split = place.toString().split(" (?i)OR ");
					if(split.length >= 3) {
						System.out.println("OR cycle(hard coded type) has been detected!");
	                }*/
					auto = Automaton.makeString(place.toString());
				} else {
					// this case should not happen any longer (now that
					// we have "uninit" nodes, see below)
					throw new RuntimeException("SNH: " + place + ", " + normalNode.getCfgNode().getFileName() + "," + normalNode.getCfgNode().getOriginalLineNumber());
				}
			} else {
				// this is an interior node, not a leaf node;
				// the automaton for this node is the union of all the
				// successor automatas
				for (AbstractNode succ : successors) {
					if (succ == node) {
						// a simple loop, can be ignored
						continue;
					}
					Automaton succAuto = deco.get(succ);
					if (succAuto == null) {
						throw new RuntimeException("SNH");
					}
					if (auto == null) {
						auto = succAuto;
					} else {
						auto = auto.union(succAuto);
					}
				}
			}
		} else if (node instanceof BuiltinFunctionNode) {
			auto = this.makeAutoForOp((BuiltinFunctionNode) node, deco, dependencyGraph);
		} else if (node instanceof CompleteGraphNode) {
			// for SCC nodes, we generate a coarse string approximation (.* automaton);
			// the taint value depends on the taint value of the successors:
			// if any of the successors is tainted in any way, we make the resulting
			// automaton tainted as well
			/*
			 * * this approach works under the assumption that the SCC contains
			 * * no "evil" functions (functions that always return a tainted value),
			 * * and that the direct successors of the SCC are not <uninit> nodes
			 * * (see there, below);
			 * * it is primarily based on the fact that SCCs can never contain
			 * * <uninit> nodes, because <uninit> nodes are always leaf nodes in the dependency
			 * * graph (since they have no successors);
			 * * under the above assumptions and observations, it is valid to
			 * * say that the taint value of an SCC node solely depends on
			 * * the taint values of its successors, and that is exactly what we
			 * * do here
			 * *
			 */
			//Transition.Taint taint = Transition.Taint.Untainted;
			for (AbstractNode successor : successors) {
				//System.out.println(successor.toString());
				if (successor == node) {
					// a simple loop, should be part of the SCC
					throw new RuntimeException("SNH");
				}
				Automaton succAuto = deco.get(successor);
				if (succAuto == null) {
					throw new RuntimeException("SNH");
				}
				//not care about vulnerability stuff
				/*if (succAuto.hasTaintedTransitions()) {
					taint = Transition.Taint.Directly;
					//break;
				}*/
				Set<AbstractNode> origPreds = origDependencyGraph.getPredecessors(successor);
				if(origPreds.size() == 1) {
					AbstractNode origPre = origPreds.iterator().next();
					if(origPre instanceof BuiltinFunctionNode && ((BuiltinFunctionNode) origPre).getName().equals(".")) {
						succAuto = succAuto.repaet(succAuto);
						//System.out.println(succAuto.toString());
						//succAuto.toString();
						//Check if there is a "OR" loop in the automaton
						//if(succAuto.hasOrs()) {
						//	System.out.println("OR cycle(loop type) has been detected!");
						//}
	                }
				}
				
				if(auto == null) {
					auto = succAuto;
				}else {
					auto = auto.concatenate(succAuto);
				}
			}
		} else if (node instanceof UninitializedNode) {
			// retrieve predecessor
			Set<AbstractNode> preds = dependencyGraph.getPredecessors(node);
			if (preds.size() != 1) {
				throw new RuntimeException("SNH");
			}
			AbstractNode pre = preds.iterator().next();
			if (pre instanceof NormalNode) {
				NormalNode preNormal = (NormalNode) pre;
				switch (this.getInitialTaintForPlace(preNormal.getPlace())) {
					case ALWAYS:
					case IF_REGISTER_GLOBALS:
						auto = Automaton.makeAnyString(Transition.Taint.Directly);
						break;
					case NEVER:
						auto = Automaton.makeAnyString(Transition.Taint.Untainted);
						break;
					default:
						throw new RuntimeException("SNH");
				}
			} else if (pre instanceof CompleteGraphNode) {
				// this case can really happen (e.g.: dcpportal: advertiser.php, forums.php);
				// take a look at the "real" predecessors (i.e., take a look "into"
				// the SCC node): if there is exactly one predecessor, namely a
				// NormalNode, and if the contained place is initially untainted,
				// there is no danger from here; else: we will have to set it to tainted
				Set<AbstractNode> origPreds = origDependencyGraph.getPredecessors(node);
				if (origPreds.size() == 1) {
					AbstractNode origPre = origPreds.iterator().next();
					if (origPre instanceof NormalNode) {
						NormalNode origPreNormal = (NormalNode) origPre;
						switch (this.getInitialTaintForPlace(origPreNormal.getPlace())) {
							case ALWAYS:
							case IF_REGISTER_GLOBALS:
								auto = Automaton.makeAnyString(Transition.Taint.Directly);
								break;
							case NEVER:
								auto = Automaton.makeAnyString(Transition.Taint.Untainted);
								break;
							default:
								throw new RuntimeException("SNH");
						}
					} else {
						auto = Automaton.makeAnyString(Transition.Taint.Directly);
					}
				} else {
					// conservative decision for this SCC
					auto = Automaton.makeAnyString(Transition.Taint.Directly);
				}
			} else {
				throw new RuntimeException("SNH: " + pre.getClass());
			}
		} else {
			throw new RuntimeException("SNH");
		}
		if (auto == null) {
			throw new RuntimeException("SNH");
		}
		deco.put(node, auto);
	}
	
	//Returns an automaton for the given operation node.
	//what is makeAnyString used for Transition.Taint.Untainted and Transition.Taint.Directly??? not aware now.
	private Automaton makeAutoForOp(BuiltinFunctionNode node, Map<AbstractNode, Automaton> deco, DependencyGraph dependencyGraph) {
		List<AbstractNode> successors = dependencyGraph.getSuccessors(node);
		if (successors == null) {
			successors = new LinkedList<>();
		}
		
		Automaton retMe = null;
		String opName = node.getName();
		List<Integer> multiList = new LinkedList<>();
		if (!node.isBuiltin()) {
			// call to function or method for which no definition
			// could be found
			
			AbstractCfgNode cfgNodeX = node.getCfgNode();
			if (cfgNodeX instanceof CallUnknownFunction) {
				CallUnknownFunction cfgNode = (CallUnknownFunction) cfgNodeX;
				if (cfgNode.isMethod()) {
					retMe = Automaton.makeAnyString(Transition.Taint.Untainted);
				} else {
					retMe = Automaton.makeAnyString(Transition.Taint.Directly);
				}
			} else {
				throw new RuntimeException("SNH");
			}
		} else if (opName.equals(".")) {
			// CONCAT
			for (AbstractNode succ : successors) {
				Automaton succAuto = deco.get(succ);
				if (retMe == null) {
					retMe = succAuto;
				} else {
					retMe = retMe.concatenate(succAuto);
				}
			}
			// WEAK SANITIZATION FUNCTIONS *******************************
			// ops that perform sanitization, but which are insufficient
			// in cases where the output is not enclosed by quotes in an SQL query
		} else if (isWeakSanitation(opName, multiList)) {
			retMe = Automaton.makeAnyString(Transition.Taint.Indirectly);
			
			// STRONG SANITIZATION FUNCTIONS *******************************
			// e.g., ops that return numeric values
		} else if (isStrongSanitation(opName)) {
			retMe = Automaton.makeAnyString(Transition.Taint.Untainted);
			
			// EVIL FUNCTIONS ***************************************
			// take care: if you define evil functions, you must adjust
			// the treatment of SCC nodes in decorate()
			// MULTI-OR-DEPENDENCY **********************************
		} else if (useTransducers && opName.equals("str_replace")) {
			if (successors.size() < 3) {
				throw new RuntimeException("SNH");
			}
			Automaton searchAuto = deco.get(successors.get(0));
			Automaton replaceAuto = deco.get(successors.get(1));
			Automaton subjectAuto = deco.get(successors.get(2));

			// search and replace have to be finite strings;
			// extract them
			boolean supported = true;
			String searchString = null;
			String replaceString = null;
			if (searchAuto.isFinite() && replaceAuto.isFinite()) {
				Set<String> searchSet = searchAuto.getFiniteStrings();
				Set<String> replaceSet = replaceAuto.getFiniteStrings();
				if (searchSet.size() != 1 || replaceSet.size() != 1) {
					supported = false;
				} else {
					searchString = searchSet.iterator().next();
					replaceString = replaceSet.iterator().next();
				}
			} else {
				supported = false;
			}
			if (!supported) {
				throw new RuntimeException("not supported yet");
			}

			Automaton transduced = new MyTransductions().str_replace(searchString, replaceString, subjectAuto);
			return transduced;
		} else if (isMultiDependencyOperation(opName, multiList)) {
			Transition.Taint taint = this.multiDependencyAuto(successors, deco, multiList, false);
			retMe = Automaton.makeAnyString(taint);
		} else if (isInverseMultiDependencyOperation(opName, multiList)) {
			Transition.Taint taint = this.multiDependencyAuto(successors, deco, multiList, true);
			retMe = Automaton.makeAnyString(taint);
			
			// CATCH-ALL ********************************************
		} else {
			System.out.println("Unmodeled builtin function (SQL): " + opName);
			// conservative decision for operations that have not been
			// modeled yet: .*
			retMe = Automaton.makeAnyString(Transition.Taint.Directly);
		}
		return retMe;
	}
	
	//multiDependencyAuto???
	private Transition.Taint multiDependencyAuto(List<AbstractNode> successors, Map<AbstractNode, Automaton> deco, List<Integer> indices, boolean inverse) {
		boolean indirectly = false;
		Set<Integer> indexSet = new HashSet<>(indices);

		int count = -1;
		for (AbstractNode successor : successors) {
			count++;

			// check if there is a dependency on this successor
			if (inverse) {
				if (indexSet.contains(count)) {
					continue;
				}
			} else {
				if (!indexSet.contains(count)) {
					continue;
				}
			}

			Automaton successorAutomaton = deco.get(successor);
			if (successorAutomaton == null) {
				throw new RuntimeException("SNH");
			}
			
			if (successorAutomaton.hasDirectlyTaintedTransitions()) {
				return Transition.Taint.Directly;
			}
			
			if (successorAutomaton.hasIndirectlyTaintedTransitions()) {
				indirectly = true;
			}
		}
		
		if (indirectly) {
			return Transition.Taint.Indirectly;
		} else {
			// no tainted successors have been found
			return Transition.Taint.Untainted;
		}
	}
}